festvox-ca-ona-hts (1.3-4) UNRELEASED; urgency=medium

  [ Samuel Thibault ]
  * control: Bump Standards-Version to 4.6.0 (no change)
  * control: Bump debhelper compat to 12.
  * control: Set Rules-Requires-Root to no.
  * control: Make Multi-Arch: foreign.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Remove constraints unnecessary since buster:
    + festvox-ca-ona-hts: Drop versioned constraint on festival and festival-ca
      in Depends.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 31 Jul 2020 22:51:10 +0200

festvox-ca-ona-hts (1.3-3) unstable; urgency=medium

  * control: Set Vcs-* to salsa.debian.org.
  * control: Bump Standards-Version to 4.4.0 (no changes).
  * watch: Generalize pattern.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 09 Dec 2019 01:29:12 +0100

festvox-ca-ona-hts (1.3-2) unstable; urgency=medium

  * Team upload.
  * Use canonical anonscm vcs URL.
  * control: Bump Standards-Version to 4.1.4 (no changes).

 -- Samuel Thibault <sthibault@debian.org>  Sun, 29 Apr 2018 12:09:08 +0200

festvox-ca-ona-hts (1.3-1) unstable; urgency=medium

  * New upstream version.
    - Works with festival 2.4 (Closes: #785799)
  * Bump Standards Version to 3.9.8 (no action needed)
  * Updated Vcs-* fields to use an encrypted connection.
  * Depends on festival-2.4 (reason: requires its hts engine)
  * Depends on the latest festival-ca (reason: fixes speech synthesis bugs)

 -- Sergio Oller <sergioller@gmail.com>  Mon, 05 Sep 2016 22:12:09 +0200

festvox-ca-ona-hts (1.2-1) unstable; urgency=medium

  * New upstream version.
    - Fix bugs with saying-some-words-split-like-this.
  * Updated d/copyright after discussion on debian-legal

 -- Sergio Oller <sergioller@gmail.com>  Sun, 04 May 2014 02:26:28 +0200

festvox-ca-ona-hts (1.0.2-1) unstable; urgency=low

  * Initial release (Closes: #639701)

 -- Sergio Oller <sergioller@gmail.com>  Sun, 19 Feb 2012 16:16:00 +0100
